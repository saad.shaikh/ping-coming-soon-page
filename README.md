# Title: Ping coming soon page

Tech stack: Next.js & Font Awesomw

Deployed project: https://saad-shaikh-ping-coming-soon-page.netlify.app/

## Main tasks:
- Created optimal layouts for the site for different screen sizes
- Enabled hover states for all interactive elements on the page
- Created an email address `input` field
- Enabled an error message when the `form` is submitted if the email address is not formatted correctly (i.e. a correct email address should have this structure: `name@host.tld`). The message for this error says *"Please provide a valid email address"*

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
