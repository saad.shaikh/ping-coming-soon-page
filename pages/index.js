import React, { Fragment } from "react"
import Head from 'next/head'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'

export default function Home() {
  return (
    <Fragment>
      <Head>
        <title>Ping coming soon page</title>
        <meta name="description" content="Ping coming soon page" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <header>
        <h1>
          <img src="./logo.svg" alt="PING."></img>
        </h1>
      </header>

      <main>
        <h2>We are launching <span className="emphasis">soon!</span></h2>
        <p>Subscribe and get notified</p>
        <form>
          <input id="input-email" type="email" placeholder="Your email address..." required></input>
          <span className="input-error" >Please provice a valid email address</span>
          <input type="submit" value="Notify Me"></input>
        </form>
        <img className="illustration" src="./illustration-dashboard.png" alt="Illustration Dashboard"></img>
      </main>

      <footer>
        <div className="social-links">
          <div className="social-image-border">
            <FontAwesomeIcon className="social-image" icon={faFacebookF} />
          </div>
          <div className="social-image-border">
            <FontAwesomeIcon className="social-image" icon={faTwitter} />
          </div>
          <div className="social-image-border">
            <FontAwesomeIcon className="social-image" icon={faInstagram} />
          </div>
        </div>
        <p className="copyright">&copy; Copyright Ping. All rights reserved.</p>
        <p className="attribution">
          Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank" rel="noreferrer">Frontend Mentor</a>.
          Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" target="_blank" rel="noreferrer">Saad Shaikh</a>.
        </p>
      </footer>
    </Fragment>
  )
}
